
//Our Services
const serviceTabs = document.querySelectorAll(".services-container li");
const serviceTabContents = document.querySelectorAll(".service-description-container div");
document.querySelector(".services-container").addEventListener("click", (event) => {
    for (let i = 0; i < serviceTabs.length; i++) {
        if (serviceTabs[i] == event.target) {
            serviceTabs[i].classList.add("service-item-selected");
            serviceTabContents[i].classList.add("services-content-active");
        }
        else {
            serviceTabs[i].classList.remove("service-item-selected");
            serviceTabContents[i].classList.remove("services-content-active");
        }
    }
});


//Works
function showWorksByCategory(categoryName) {
    if (categoryName === "all") {
        for (let i = 0; i < worksCounter; i++) {
            works[i].classList.remove("work-item-hidden");
        }
        return;
    }
    for (let i = 0; i < worksCounter; i++) {
        if (works[i].getAttribute("category") === categoryName) {
            works[i].classList.remove("work-item-hidden");
        }
        else {
            works[i].classList.add("work-item-hidden");
        }
    }
}

function selectCategoryTab(target) {
    for (const tab of workTabs) {
        if (tab === target) {
            tab.classList.add("work-category-item-selected");
        }
        else {
            tab.classList.remove("work-category-item-selected");
        }
    }
}

function showMoreWorks() {
    this.style.display = "none";
    const loader = document.querySelector("#works-loader");
    loader.classList.add("loader-active");
    worksCounter += 12;
    setTimeout(() => {
        loader.classList.remove("loader-active");
        if (worksCounter === 36) {
            this.style.display = "none";
        }
        else {
            this.style.display = "block";
        }
        showWorksByCategory(savedCategory);
    }, 2000);
}

let worksCounter = 12;
document.getElementById("works-more-btn").addEventListener("click", showMoreWorks);

const works = document.querySelectorAll(".work-item");
const workTabs = document.querySelectorAll(".work-category-item");
let savedCategory = "all";
document.querySelector(".work-categories-container").addEventListener("click", (event) => {
    if (event.target.innerText.length < 30) {
        selectCategoryTab(event.target);
        savedCategory = event.target.getAttribute("category");
        showWorksByCategory(event.target.getAttribute("category"));
    }
});

// Review Slider
function makeActive(activeElement) {
    for (const element of smallPhotos) {
        if (element.classList.contains("active-photo")) {
            previousActiveElement = element;
        }
    }
    for (const element of smallPhotos) {
        if (element === activeElement) {
            activeElement.classList.add("active-photo");
            const personIndex = activeElement.dataset.personId;
            reviewText.innerText = persons[personIndex].message;
            nameElement.innerText = persons[personIndex].name;
            profession.innerText = persons[personIndex].position;
            bigPhoto.src = persons[personIndex].photo;
            continue;
        }
        if (element === previousActiveElement) {
            element.classList.remove("active-photo");
        }
    }
}

function slider(event, isNext) {
    if (event != null) {
        if (isNext) {
            firstSlide++;
        }
        else {
            firstSlide--;
        }
    }
    else {
        firstSlide = 0;
    }
    if (firstSlide === persons.length) {
        firstSlide = 0;
    }
    if (firstSlide === -1) {
        firstSlide = persons.length - 1;
    }
    let photoInArray = firstSlide;
    for (let i = 0; i < smallPhotos.length; i++) {
        smallPhotos[i].src = persons[photoInArray].photo;
        smallPhotos[i].dataset.personId = photoInArray;
        if (i == activeElement) {
            makeActive(smallPhotos[i]);
        }
        photoInArray++;
        if (photoInArray === persons.length) {
            photoInArray = 0;
            continue;
        }
        if (photoInArray === 0) {
            photoInArray = persons.length - 1;
        }
    }
}

const persons = [
    {
        name: "Daenerys Targaryen",
        position: "Mother of dragons",
        message: "Drakaris! This step-project perfectly reflects the abilities of a DAN.iT student",
        photo: "img/review/Daenerys_Targaryen.jpg"
    },
    {
        name: "Tyrion Lannister",
        position: "The god of tithek and wine",
        message: "Lannisters always pay their debts",
        photo: "img/review/Tyrion_Lannister.jpg"
    },
    {
        name: "Theon Greyjoy",
        position: "Youngest son and heir of Balon Greyjoy",
        message: "What is dead cannot die",
        photo: "img/review/Theon_Greyjoy.jpg"
    },
    {
        name: "Arya Stark",
        position: "Faceless",
        message: "What do we say to the God of death? - Not today!",
        photo: "img/review/Arya_Stark.jpg"
    },
    {
        name: "Cersei Lannister",
        position: "Queen of the Seven Kingdoms of Westeros",
        message: "I choose violence",
        photo: "img/review/Cersei_Lannister.jpg"
    },
    {
        name: "Jaime Lannister",
        position: "Knight of the Kingsguard",
        message: "One of the things I wanted to explore with Jaime, and with so many of the characters, is the whole issue of redemption. When can we be redeemed? Is redemption even possible? ... When do we forgive people?",
        photo: "img/review/Jaime_Lannister.jpg"
    },
    {
        name: "Ramsay Bolton",
        position: "Bastard",
        message: "Ramsay is the product of rape. While hunting along the Weeping Water, Roose Bolton saw a miller's wife and decided to illicitly practice the banned tradition of 'the first night', wherein a lord had the right to bed a commoner's bride.",
        photo: "img/review/Ramsay_Bolton.jpg"
    },
    {
        name: "Sansa Stark",
        position: "Second child of Lord Eddard Stark",
        message: "Sansa welcomes Daenerys and her court, including Tyrion, to Winterfell. Tyrion declares that the Lannister troops will be marching north as well to defend against the dead, but Sansa is skeptical.",
        photo: "img/review/Sansa_Stark.jpg"
    }
];

const reviewText = document.querySelector(".review-text");
const nameElement = document.querySelector(".review-name");
const profession = document.querySelector(".review-profession");
const bigPhoto = document.querySelector(".review-big-photo");
const smallPhotos = document.querySelectorAll(".review-small-photo");
const prevBtn = document.getElementById("prevArrow");
const nextBtn = document.getElementById("nextArrow")
let firstSlide;
let activeElement = 0;
let previousActiveElement;

slider();

prevBtn.addEventListener("click", (event) => slider(event, false));
nextBtn.addEventListener("click", (event) => slider(event, true));
document.querySelector(".review-photo-container").addEventListener("click", (event) => {
    if (event.target.classList.contains("review-small-photo")) {
        const selectedElement = event.target;
        makeActive(selectedElement);
        activeElement = selectedElement.dataset.position;
    }
});
